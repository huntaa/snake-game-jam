#pragma once

#include <iostream>
#include <SDL2/SDL.h>
#include <concepts>
#include <functional>
#include <algorithm>

namespace GameEngine {

/* this is the externally defined entry point of the game */
bool game_code();
static constexpr int grid_size = 24;
class Engine {
  SDL_Renderer *renderer;
  SDL_Window *window;

  /* Draws all objects to the window */
  bool render(SDL_Window *window, SDL_Renderer *renderer);

public:
  /* Calls game_code() then render() 60 times per second */
  bool game_loop();

  bool init();

  ~Engine();
};
}


// use concepts when referncing Derived type to ensure Return type is defined

namespace Events {


    template<typename T, int... priorities>
    concept EventInterface = requires(T EventType) {
        
        // returns an integer indicating it's priority
        {EventType.subscribe(priorities...)} -> std::same_as<int>;
    };
    

    class Event {
    protected:
        int priority;
    public:
        constexpr Event() {};
        constexpr Event(int val): priority{val} {};
        virtual void run() = 0;
    };
    

    /// takes type T as a template parameter where T is the system's corresponding component type.
    template<typename T, EventInterface... E>
    class Derived : Event {
    
    public:
        constexpr Derived(E... events): Event(subscribe(events...)) {};
        //Derived(): Event() {};

        virtual void run() override {
            std::cout<<"Priority: "<<priority<< std::endl;
        }

        /** could change this to be an std::bind  */
        template<EventInterface ...priorities>
        //constexpr int subscribe(priorities... args) { return 1 + (callSubscribe(args) + ...);};
        constexpr int subscribe(priorities... args) { return 1 + callSubscribe(args...);};

        constexpr int subscribe() {return 1;}

        template<EventInterface I>
        constexpr int callSubscribe(I event) { return event.subscribe();}

        template<EventInterface I, EventInterface... Is>
        constexpr int callSubscribe(I event, Is... events) {return std::max(event.subscribe(), callSubscribe(events...));}
        //constexpr int callSubscribe(I event) {return event.subscribe();}

    };


    /// prints something
    void printSomething(EventInterface auto event) {
        std::cout<<"something"<<std::endl;
    }
}


/*
namespace Events {

    class Event {
        int priority;
    public:
        Event(int val): priority{val} {};
        virtual void run() = 0;
    };


    class Derived : Event {
    
    public:
        Derived();

        virtual void run() override {
            Return result;
            std::cout<<"result: "<< result.val1 + result.val2;
        }

        // could change this to be a std::bind  
        constexpr int subscribe();


        class Return {
        public:
            float val1;
            float val2;
            Return() {};
        };

    };


    template<typename T>
    concept EventInterface = requires(T EventType) {
        typename T::Return;
        // returns an integer indicating it's priority
        {EventType.subscribe()} -> std::same_as<int>;
    };
    
    void printSomething(EventInterface auto event) {
        std::cout<<"something"<<std::endl;
    }
}


*/
