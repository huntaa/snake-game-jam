#include "engine.hpp"
#include <SDL2/SDL_video.h>

using namespace GameEngine;

int main() {
    GameEngine::Engine engine;
    engine.init();
    engine.game_loop();
    return 0;
}    

bool Engine::init() {
	
    SDL_Init(SDL_INIT_EVERYTHING);
	
    window = SDL_CreateWindow(
		"snaek", 
		SDL_WINDOWPOS_CENTERED, 
		SDL_WINDOWPOS_CENTERED, 
		grid_size * 61, 
		61 * grid_size,
		0
	);

	renderer = SDL_CreateRenderer(
            window, 
            -1, 
            SDL_RENDERER_SOFTWARE
    );
    
    return true;
}


bool Engine::game_loop() {
    SDL_Event Ev;
    do {
        // handle all sdl events then run game logic 
        while(SDL_PollEvent(&Ev)) {
            switch(Ev.type) {
                case SDL_QUIT:
                    return 1;
                
            }
        }
    } 
    while(game_code());
    return 1;
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

bool Engine::render(SDL_Window* window, SDL_Renderer* renderer) {
    return true;
}
