#include <iostream>
#include "engine.hpp"

struct NotEvent {
    int a;
};

bool GameEngine::game_code() {
    typedef Events::Derived<int> IntEvent;
    Events::Derived<int> derived {};
    typedef Events::Derived<float> FloatEvent;
    Events::Derived<float> derv {};

    typedef Events::Derived<char, IntEvent, FloatEvent> CharEvent;
        CharEvent der = CharEvent(derived, derv);

    //std::cout<<derv.subscribe(derived, der)<<std::endl;
    Events::printSomething(derived);
    der.run();
    return 1;
}
