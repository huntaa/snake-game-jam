DEBUG := 1
project_name = SnakeGame
libraries = sdl2

source_files = $(wildcard src/*.cpp)    #need to use wildcard function when line is not a rule
objs = $(patsubst src/%.cpp, .objs/%.o, $(source_files))    #substituting '.cpp' for '.o'
dependancies = $(patsubst src/%.cpp, .deps/%.d, $(source_files))

#contains the arguments required to link the libraries, defined in 'libraries', and their dependencies
libs = $(shell pkg-config --libs $(libraries))

ifeq ($(DEBUG), 1)
	CFLAGS = -O0 -g --std=c++20 -Iinclude
	mode = debug
else
	CFLAGS = -O3 --std=c++20 -Iinclude
	mode = release
endif

output_dir = build/$(mode)

CXX=clang++

$(shell mkdir -p .deps 2>/dev/null)
$(shell mkdir -p .objs 2>/dev/null)

$(shell mkdir -p $(output_dir) 2>/dev/null)

$(output_dir)/$(project_name): $(objs)
	$(CXX) $(CFLAGS) $(LINKFLAGS)  $^ -o $@ $(libs)

.objs/$(notdir %.o): src/%.cpp
	$(CXX) $(CFLAGS)  -MF $(patsubst %.cpp, .deps/%.d, $(notdir $<)) -MMD -c -o $@ $<

-include $(dependancies)

.PHONY:all
all:$(output_dir)/$(project_name)

.PHONY:run
run:all
	$(output_dir)/$(project_name)

.PHONY:clean
clean:
	rm -rf .deps .objs build >/dev/null 2>&1
